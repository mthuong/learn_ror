class EnterValueClass
  
  def initialize(argv)
    @x = argv
  end
  
  def x
    @x
  end
  
end

# x = 5;
# if x > 10 then puts x end
#
# if x > 10; puts x; end
#
print "Enter an integer: "
n = gets.to_i
while n <= 0
  print "Enter an positive integer: "
  
  n = gets.to_i
end