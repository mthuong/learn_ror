a = [ 3.14159, "pie", 99 ] 

puts a.class # => Array 
puts a.length # => 3
puts a[0] # => 3.14159
puts a[1] # => "pie"
puts a[2] #=>99
puts a[3] #=>nil
puts a[-1] #=>99
puts a[-2] #=> "pie"
puts a[-3] #=> 3.14159