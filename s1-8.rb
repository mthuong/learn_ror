n = 3

name = "Thuong"

def print_name_n(name, n)
  
  i = 0
  while i < n
    yield(name)
    i += 1
  end
  
end

print_name_n(name, n) { |x| puts x }